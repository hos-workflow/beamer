#!/usr/bin/env lua5.4

-- themes {{{
local scheme = {

	-- manjaro {{{
	manjaro = {
		text = "#ffffff",
		page = "#000000",
		cf = "#dddddd",
		cg = "#05221d",
		bg = "#000000",
		block = "#083930",
		line_1 = "#083930",
		line_2 = "#05221d",
		line_bg = "#ffffff",
		gradiant = "#000000",
		item = "#34b35b",
		main = "#8ae234",
		side = "#ffffff",
		side_shade = "#cccccc",
		title = "#0a4b3e",
		example = "#729fcf",
		alert = "#cc6666",
	},
	-- }}}

	-- ubuntu {{{
	ubuntu = {
		text = "#010101",
		page = "#ffffff",
		cf = "#242424",
		cg = "#2c001e",
		bg = "#ccccce",
		block = "#eeedeb",
		line_1 = "#5e2750",
		line_2 = "#e95420",
		line_bg = "#ffffff",
		gradiant = "#000000",
		item = "#772953",
		main = "#2d0922",
		title = "#e95420",
		side = "#ffffff",
		side_shade = "#cccccc",
		example = "#005577",
		alert = "#770000",
	},
	-- }}}

	-- solarized {{{
	solarized = {

		-- light {{{
		light = {
			text = "#002b36",
			page = "#000000",
			cf = "#073642",
			bg = "#fdf6e3",
			cg = "#000000",
			block = "#eee8d5",
			line_1 = "#073642",
			line_2 = "#002b36",
			line_bg = "#fdf6e3",
			gradiant = "#000000",
			item = "#d33682",
			main = "#859900",
			side = "#fdf6e3",
			side_shade = "#eee8d5",
			title = "#073642",
			example = "#268bd2",
			alert = "#dc322f",
		},
		-- dark }}}

		-- dark {{{
		dark = {
			text = "#fdf6e3",
			page = "#000000",
			cf = "#eee8d5",
			cg = "#002b36",
			bg = "#000000",
			block = "#002b36",
			line_1 = "#073642",
			line_2 = "#002b36",
			line_bg = "#fdf6e3",
			gradiant = "#000000",
			item = "#d33682",
			main = "#b58900",
			side = "#fdf6e3",
			side_shade = "#eee8d5",
			title = "#859900",
			example = "#268bd2",
			alert = "#dc322f",
		},
		-- dark }}}

	},
	-- }}}

	-- hybrid {{{
	hybrid = {

		-- light {{{
		light = {
			text = "#000000",
			page = "#f1fbf7",
			bg = "#c1cbc7",
			cg = "#f1fbf7",
			cf = "#232323",
			block = "#e1ebe7",
			line_1 = "#717b77",
			line_2 = "#515b57",
			line_bg = "#ffffff",
			gradiant = "#000000",
			item = "#5f005f",
			side = "#ffffff",
			side_shade = "#c1cbc7",
			title = "#a1aba7",
			main = "#00005f",
			example = "#005f5f",
			alert = "#5f0000",
		},
		-- }}}

		-- dark {{{
		dark = {
			page = "#1d1f21",
			text = "#ffffff",
			bg = "#777b81",
			cg = "#e4e4e4",
			cf = "#1d1f21",
			block = "#e4e4e4",
			line_1 = "#575b61",
			line_2 = "#373b41",
			line_bg = "#e4e4e4",
			gradiant = "#000000",
			item = "#b294bb",
			side = "#e4e4e4",
			side_shade = "#cccccc",
			title = "#373b41",
			main = "#81a2be",
			example = "#b5bd68",
			-- main = "#f0c674",
			-- example = "#81a2be",
			alert = "#cc6666",
		},
		-- }}}

	},
	-- }}}

	-- basics {{{
	basics = {

		blue = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#EAEAF3",
			block_eg    = "#E6EFE6",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#3131AB",
			side_shade  = "#cccccc",
			item        = "#191959",
			main        = "#262686",
			title       = "#3333B3",
			line_1      = "#3131AB",
			line_2      = "#262686",
			example     = "#006000",
			alert       = "#BF0000",
		},

		green = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#E6EFE6",
			block_eg    = "#EAEAF3",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#E6EFE6",
			side_shade  = "#cccccc",
			item        = "#003B00",
			title       = "#007700",
			line_1      = "#007700",
			line_2      = "#005900",
			main        = "#005900",
			example     = "#262686",
			alert       = "#BF0000",
		},

		orange = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#F9EFE6",
			block_exam  = "#E6EFE6",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#F9EFE6",
			side_shade  = "#cccccc",
			item        = "#803B00",
			main        = "#bf5900",
			title       = "#ff7700",
			line_1      = "#ff7700",
			line_2      = "#bf5900",
			example     = "#006000",
			alert       = "#BF0000",
		},

		sblue = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#E6EFEC",
			block_exam  = "#E6EFE6",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#E6EFEC",
			side_shade  = "#cccccc",
			item        = "#002A3B",
			main        = "#004059",
			title       = "#005577",
			line_1      = "#005577",
			line_2      = "#004059",
			example     = "#006000",
			alert       = "#BF0000",
		},


		spurple = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#F9EFE6",
			block_exam  = "#E6EFE6",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#777777",
			side_shade  = "#cccccc",
			item        = "#3B002A",
			main        = "#590040",
			title       = "#550077",
			line_1      = "#550077",
			line_2      = "#590040",
			example     = "#006000",
			alert       = "#BF0000",
		},

		sred = {
			gradiant    = "#000000",
			text        = "#010101",
			cf          = "#242424",
			page        = "#ffffff",
			line_bg     = "#ffffff",
			side        = "#ffffff",
			block       = "#EFE6E6",
			block_exam  = "#E6EFE6",
			block_alert = "#F9E6E6",
			bg          = "#dcdcdc",
			cg          = "#EFE6E6",
			side_shade  = "#cccccc",
			item        = "#3B0000",
			main        = "#590000",
			title       = "#770000",
			line_1      = "#770000",
			line_2      = "#590000",
			example     = "#006000",
			alert       = "#BF0000",
		},

	},
	-- }}}

	-- void {{{
	void = {
		gradiant    = "#232323",
		text        = "#010101",
		cf          = "#242424",
		page        = "#ffffff",
		line_bg     = "#ffffff",
		side        = "#ffffff",
		block       = "#E9ECEB",
		block_eg    = "#E8E9EB",
		block_alert = "#EBE9E8",
		bg          = "#dcdcdc",
		cg          = "#E9ECEB",
		side_shade  = "#cccccc",
		item        = "#142920",
		title       = "#295340",
		line_1      = "#295340",
		line_2      = "#1E3E30",
		main        = "#1E3E30",
		example     = "#1E303E",
		alert       = "#2E2416",
	},
	-- }}}

	-- debian {{{
	debian = {
		gradiant    = "#000000",
		text        = "#010101",
		cf          = "#242424",
		page        = "#ffffff",
		line_bg     = "#ffffff",
		side        = "#ffffff",
		bg          = "#dcdcdc",
		cg          = "#E9ECEB",
		side_shade  = "#cccccc",
		item        = "#540018",
		title       = "#A80030",
		line_1      = "#A80030",
		line_2      = "#7E0024",
		-- 
		main        = "#7E0024",
		block       = "#E9ECEB",
		example     = "#00247E",
		block_eg    = "#E6E9F2",
		alert       = "#1A1A1A",
		block_alert = "#E8E8E8",
	},
	-- }}}

	-- TODO: add template to create new schemes from

}
-- }}}

--[[ change themes => `scheme.<name>` 
	name:
		+ `ubuntu`              + `debian`
		+ `manjaro`             + `void`
		+ `hybrid.dark`         + `hybrid.light`
		+ `solarized.dark`      + `solarized.light`
		+ `basics.blue`         + `basics.green`
		+ `basics.orange`       + `basics.sblue`
		+ `basics.spurple`      + `basics.sred`
--]]

local current = scheme.debian

-- set element colors {{{
local colors = {
	color_main = current.main,
	color_page = current.bg,
	color_page_gradiant = current.cg,
	color_text = current.text,
	color_frametitle_fg = current.text,
	color_but_right_fg = current.text,
	color_but_mid_fg = current.text,
	color_but_left_fg = current.text,
	color_block_body_fg = current.cf,
	color_gradient = current.gradiant,
	color_items_fg = current.page,
	color_items_bg = current.item,
	color_frametitle_bg = current.title,
	color_line_fg = current.line_bg,

	color_block_def_bg = current.block_body or current.block,
	color_block_exm_bg = current.block_exam or current.block,
	color_block_alr_bg = current.block_alert or current.block,

	color_but_left_bg = current.title,
	color_but_mid_bg = current.line_2,
	color_but_right_bg = current.line_1,

	color_sidebar_bg = current.side,
	color_sidebar_shade_bg = current.side_shade,

	color_block_def_title_bg = current.main,
	color_example_title_bg = current.example,
	color_alert_title_bg = current.alert,

	color_blocks_title_fg = current.page,
}
-- }}}

local theme_file = "theme.tex"
local file = io.open(theme_file, "w")
if file == nil then
	io.stderr:write(string.format("%s: Cannot open\n", theme_file))
	os.exit(2)
end

for key, value in pairs(colors) do
	local stripped = string.gsub(value, "^#", "")
	file:write(string.format("\\definecolor{%s}{HTML}{%s} %% %s\n", key, stripped, value))
	-- io.write(key, " ", value, "\n")
end
file:close()

